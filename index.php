<!DOCTYPE html>
<html lang="en">
<head>
  <title>Bootstrap Example</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>
<form action="#" method="post" id="checkForm" name="checkForm" target="_blank" onsubmit="return false;">
  <div class="container">
	<div class="progress">
		<div class="progress-bar" role="progressbar" id='myprogressbar' name='myprogressbar' aria-valuenow='0' aria-valuemin="0" aria-valuemax="100" style="width:0%">
		<span id='myprogressbartext'></span>
		</div>
	</div>
	
	<div>
	  <input type="button" class="btn btn-primary" id="btnPrev" value="Previous" class="button" style=";width: 100px"/>
	  <input type="button" class="btn btn-success" id="btnNext" value="Next" class="button" style=";width: 60px"/>
	</div>
  </div>
</form>
<script>
$(function() {
	$("#btnPrev").click(function(){
		var progVal = $('#myprogressbar').attr('aria-valuenow'); 
		progVal = parseInt(progVal) - 25;
		if (progVal < 0) {
			progVal = 0;
		}
		$('#myprogressbar').attr('aria-valuenow', progVal).css('width',progVal+'%');
		$('#myprogressbartext').text(progVal+'%');
	});
	
	$("#btnNext").click(function(){
		var progVal = $('#myprogressbar').attr('aria-valuenow'); 
		progVal = parseInt(progVal) + 25;
		if (progVal > 100) {
			progVal = 100;
		}
		$('#myprogressbar').attr('aria-valuenow', progVal).css('width',progVal+'%');
		$('#myprogressbartext').text(progVal+'%');
	});
});
</script>
</body>
</html>
